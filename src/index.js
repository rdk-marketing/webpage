import "./es/style"
import SVG_PLAY from "./svg/play.svg"
import SVG_PAUSE from "./svg/pause.svg"
import SVG_WAVE from "./svg/wave.svg"
import Blog from "./es/blog"

const hero = document.querySelector(".hero")
const campaign = document.querySelector("#kampanie_reklamowe")
const landing = document.querySelector("#landing")
const spots = document.querySelector("#spoty_reklamowe")
const marketing = document.querySelector("#content_marketing")
const contactData = document.querySelectorAll("#kontakt article")
const contact = document.querySelector("#kontakt")
const blog = document.querySelector("#blog")
const sections = document.querySelectorAll("section")
const about = document.querySelector("#o_nas")
const mobile_nav = landing.querySelector("nav.mobile")
const nav = landing.querySelector("nav")
const menuIcon = nav.querySelector(".icon.menu")
const closeIcon = mobile_nav.querySelector(".icon.close")
const links = landing.querySelectorAll("nav a")

const player = {
    self: spots.querySelector(".player"),
    icon: spots.querySelector(".player .icon"),
    wave: spots.querySelector(".player .wave"),
    ghost: document.createElement("div"),
    setWave(SVG_WAVE) {
        this.wave.innerHTML = SVG_WAVE
    },
    setIcon(SVG_ICON) {
        this.icon.innerHTML = SVG_ICON
    },
    controller: new Audio()
}

const blog_lib = new Blog()

blog_lib.onResize = extended => {
    setSectionsVisibility(!extended)
    viewportResizeHandler()
}

function setSectionsVisibility(visible) {
    let sections = document.querySelectorAll("section:not(#landing):not(#blog):not(#kontakt)")
    sections.forEach(section => {
        if(visible) {
            section.removeAttribute("style")
        } else {
            section.style.display = "none"
        }
    })
}

async function fetchAudio() {
    const request = new Request("static/mp3/mixdown.mp3")
    let response = await fetch(request)
    return URL.createObjectURL(await response.blob())
}

function isAudioSet() {
    return player.controller.src.length > 0
}

player.setIcon(SVG_PLAY)
player.setWave(SVG_WAVE)

player.ghost.setAttribute("class", "ghost")
player.wave.append(player.ghost)

async function audioPlayHandler() {  
    if(!isAudioSet()) {
        let url = await fetchAudio()
        player.controller.src = url
    }
    player.controller.play()
    player.setIcon(SVG_PAUSE)
    player.icon.removeEventListener("click", audioPlayHandler)
    player.icon.addEventListener("click", audioPauseHandler)
}

function audioPauseHandler() {    
    player.controller.pause()
    player.setIcon(SVG_PLAY)
    player.icon.removeEventListener("click", audioPauseHandler)
    player.icon.addEventListener("click", audioPlayHandler)
}

function audioProcedHandler() {
    let percent = Math.floor(player.controller.currentTime / player.controller.duration * 100)
    player.ghost.style.width = `${percent}%`
}

player.controller.addEventListener("timeupdate", audioProcedHandler)
player.controller.addEventListener("ended", audioPauseHandler)
player.icon.addEventListener("click", audioPlayHandler)


links.forEach(link => link.addEventListener("click", () => {
    setSectionsVisibility(true)
    blog_lib.fullViewCloseHandler()
    if(mobile_nav.classList.contains("visible")) {
        hideMobileMenu()
    }
}))

menuIcon.addEventListener("click", () => {
    showMobileMenu()
})

closeIcon.addEventListener("click", () => {
    hideMobileMenu()
})

function showMobileMenu() {
    mobile_nav.classList.add("visible")
}
function hideMobileMenu() {
    mobile_nav.classList.remove("visible")
}

function getScreenRatio() {
    return window.innerWidth / window.innerHeight
}

function showLinks() {
    const links = nav.querySelector(".links")
    links.classList.add("visible")
}

function hideLinks() {
    const links = nav.querySelector(".links")
    links.classList.remove("visible")
}

function shoMenuIcon() {
    menuIcon.classList.add("visible")
}

function hideMenuIcon() {
    menuIcon.classList.remove("visible")
}

contactData.forEach(article => {
    const h3 = article.querySelector("h3")
    h3.addEventListener("click", () => {
        article.classList.toggle("active")
    })
})

function setHeroOpacity(value) {
    hero.style.opacity = value
}

function adjustHeroOpacity() {
    const ratio = getScreenRatio();
    if( ratio < 1.6 ) {        
        setHeroOpacity(.15)
        if( ratio < 0.6 ) {
            setHeroOpacity(.05)
        } 
    } else {
        setHeroOpacity(1)
    }
}

function resizeCampaign() {
    const ratio = getScreenRatio();
    const main = campaign.querySelector("main");
    const svg = campaign.querySelector(".ads svg");
    if( ratio < 1.2 ) {        
        main.style.maxWidth = "100%"
        svg.style.maxHeight = "60em"
    } else {
        main.style.maxWidth = "100ch"
        svg.style.maxHeight = "50em"
    }
}

function resizeSpots() {
    const ratio = getScreenRatio();
    const main = spots.querySelector("main");
    if( ratio < 1.2 ) {        
        main.style.maxWidth = "100%"
    } else {
        main.style.maxWidth = "90ch"
    }
}

function resizeMarketing() {
    const ratio = getScreenRatio();
    const main = marketing.querySelector("main");
    if ( ratio < 1.4 ) {        
        main.style.maxWidth = "100%"
        main.style.minWidth = "0"
    } else {
        main.style.maxWidth = "100ch"
        main.style.minWidth = "80ch"
    }
}

function resizeContact() {
    const ratio = getScreenRatio();
    const main = contact.querySelector("main")
    const aside = contact.querySelector("aside")
    const card = contact.querySelector(".card")
    if( ratio < 1.2 ) {        
        main.style.margin = "0 0 5rem"
        main.style.width = "100%"
        if(ratio < .8) card.style.fontSize = "1.5vw"
        aside.style.maxWidth = "100%"
        aside.style.width = "100%"
    } else {
        main.style.margin = "0 5rem 5rem 0"
        main.style.width = "min-content"        
        card.style.fontSize = "1em"
        aside.style.maxWidth = "30%"
    }
}

function adjustLanding() {
    const ratio = getScreenRatio();
    const main = landing.querySelector("main")
    const button = main.querySelector(".button")

    if(ratio < 1.5) {
        hideLinks()
        shoMenuIcon()
    } else {
        hideMenuIcon()
        showLinks()
        hideMobileMenu()
    }
    if( ratio < .8 ) {
        main.style.maxWidth = "100%";
        button.style.margin = "1em auto 0";
    } else {
        button.style.margin = "1em 0";
        main.style.maxWidth = "100ch";
    }
}

function resizeAbout() {
    const ratio = getScreenRatio();
    const tiles = about.querySelector(".tiles")
    if( ratio < 1.3 ) {
        tiles.style.gridTemplateColumns = "1fr"
    } else {
        tiles.style.gridTemplateColumns = "1fr 1fr 1fr"
    }
}

function resizeBlog() {
    const ratio = getScreenRatio()
    const main = blog.querySelector("main")
    const content = main.querySelector(".content")
    if(ratio < 1.3 || blog_lib.isFull) {
        main.style.minWidth = "100%"
        content.style.textAlign = "justify"        
    } else {
        main.style = ""
        content.style = ""
    }

}

function setSmoothScrolling() {
    const ratio = getScreenRatio();
    const body = document.querySelector("body")
    body.style.scrollBehavior = ratio > .7 ? "smooth" : "auto"
}

function viewportResizeHandler() {
    resizeCampaign()
    adjustHeroOpacity()
    resizeSpots()
    resizeMarketing()
    resizeContact()
    adjustLanding()
    resizeAbout()
    resizeBlog()
    setSmoothScrolling()
    return viewportResizeHandler
}

function fireHiroAnimations() {
    const parts = hero.childNodes
    setTimeout(() => {
        parts.forEach((part, i) => {
            setTimeout(() => {
                part.classList.add("extended")
            }, i * 100)
        })
    }, 260)
}

function armAnimations() {
    sections.forEach(section => {
        section.classList.add("invertedAnimation")
    })    
    hero.childNodes.forEach(child => {
        child.classList.remove("extended")
    })
    fireHiroAnimations()
}

function getHeights() {
    const sections = {
        heights: [...document.querySelectorAll("section")].map(child => child.offsetTop)
    }
    return sections
}

function documentScrollHandler() {
    let heights = getHeights().heights
    heights.forEach((height, i) => {
        if( document.body.scrollTop - 300 > height) {
            if(i + 1 < document.body.children.length) {
                sections[i + 1].classList.remove("invertedAnimation")
                if(sections[i + 1].id == "blog") blog_lib.load()
            }
        }
    })
}

window.addEventListener("resize", viewportResizeHandler())
document.body.addEventListener("scroll", documentScrollHandler)

window.addEventListener("load", () => {
    if(getScreenRatio() > 1) {
        armAnimations()
    }
})