import {request, gql} from "graphql-request"
import SVG_RETURN from "../svg/return.svg"
import SVG_IMAGE from "../svg/image.svg"

const {GRAPH_API: api_url} = process.env

async function fetchEntries() {
    const entriesQuery = gql`
        query Entries {
            posts(where:{index_lt: 5}) {
                index,
                title,
                date
            }
        }
    `
    let result = await request(api_url, entriesQuery)
    return result
}

async function fetchImage(url) {
    let response = await fetch(url)
    return URL.createObjectURL(await response.blob())
}

async function fetchArticle(index) {
    const entriesQuery = gql`
        query Entries {
            posts(where:{index: ${index}}) {
                title,
                date,
                author {
                    name
                },
                short,
                content {
                    html
                },
                image {
                    url
                }
            }
        }
    `
    let result = await request(api_url, entriesQuery)
    result = result.posts[0]
    result.img = await fetchImage(result.image.url)
    result.author = result.author.name
    result.content = result.content.html
    return result
}

class FunctionButton {
    constructor(element) {
        this.self = element
        this.fullViewOpenHandler = new Function()
        this.fullViewCloseHandler = new Function()
    }
    setAsRead() {
        this.self.innerHTML = "Czytaj dalej"
        this.self.removeEventListener("click", this.fullViewCloseHandler)
        this.self.addEventListener("click", this.fullViewOpenHandler)
    }
    setAsReturn() {
        this.self.innerHTML = 'Wróć' + SVG_RETURN
        this.self.removeEventListener("click", this.fullViewOpenHandler)
        this.self.addEventListener("click", this.fullViewCloseHandler)
    }
}

class Article {
    constructor() {
        this.self = document.querySelector("#blog article")
        this.img = this.self.querySelector("img")
        this.title = this.self.querySelector("h3")
        this.author = this.self.querySelector(".author")
        this.date = this.self.querySelector(".date span")
        this.content = this.self.querySelector(".content")
        this.button = new FunctionButton(this.self.querySelector("button"))

        this.text = {
            short:"", long:""
        }

        this.setImagePlaceholder("./static/svg/image.svg")
    }

    setData({img, title, author, content, short, date}) {
        this.img.src = img
        this.title.innerHTML = title
        this.author.innerHTML = author
        this.date.innerHTML = date
        this.text.short = short + " [...]"
        this.text.long = content
        this.showLong(false)
    }

    showLong(bool) {
        this.content.innerHTML = bool ? this.text.long : this.text.short
    } 

    setImagePlaceholder(url) {
        this.img.style.backgroundImage = `url("${url}")`
    }
}

class Entry {
    constructor(index, title, date, onClick) {
        this.self = document.createElement("li")
        this.self.innerHTML = `${title}<span>${date}</span>`
        this.self.addEventListener("click", () => {
            onClick(index)
            this.setActive(true)
        })
    }
    setActive(bool) {
        this.self.classList[bool ? "add" : "remove"]("active")
    }
}

class Entries {
    constructor() {
        this.self = document.querySelector("#blog .entries")
        this.entries = []
    }
    add(entry) {
        this.entries.push(entry)
        this.self.append(entry.self)
    }
    purge() {
        this.entries = []
        let [...children] = this.self.children
        children.forEach(child => child.remove())
    }
    forEach(callback) {
        this.entries.forEach(callback)
    }
}

class Blog {
    constructor() {
        this.self = document.querySelector("#blog")
        this.entries = new Entries()
        this.article = new Article()

        this.article.button.fullViewOpenHandler = this.fullViewSelectHandler.bind(this)
        this.article.button.fullViewCloseHandler = this.fullViewCloseHandler.bind(this)
        this.article.button.setAsRead()

        this.isFull = false
        this.isLoaded = false

        this.onResize = new Function()
    }

    setFog(bool) {
        this.self.classList[bool ? "add" : "remove"]("fog")
    }
    setFullView(bool) {
        this.isFull = bool 
        this.onResize(bool)
        this.self.scrollIntoView()
        this.self.classList[bool ? "add" : "remove"]("full")  
    }

    async load() {
        if(!this.isLoaded) {
            let {posts: entries} = await fetchEntries()
            this.entries.purge()
            entries.forEach((data, i) => {
                let entry = new Entry(data.index, data.title, data.date, this.entrySelectHandler.bind(this))
                this.entries.add(entry)
                if(i == 0) {
                    entry.setActive(true)
                    fetchArticle(data.index).then(article => this.article.setData(article)).then(e => this.setFog(false))
                }
            })
            this.isLoaded = true
        }
    }

    entrySelectHandler(index) {
        this.entries.forEach(entry => {
            entry.setActive(false)
        })
        fetchArticle(index).then(article => this.article.setData(article)).finally(e => {
            this.article.showLong(this.isFull)
        })
        this.self.scrollIntoView()
    }

    fullViewSelectHandler() {
        this.article.showLong(true)
        this.setFullView(true)
        this.article.button.setAsReturn()
    }

    fullViewCloseHandler() {
        this.article.showLong(false)
        this.setFullView(false)        
        this.article.button.setAsRead()
    }
}

export default Blog


