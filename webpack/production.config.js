const {Config} = require("webpack-config")
const Copy = require("copy-webpack-plugin")
const {DIST_DIR, STATIC_DIR} = process.env

module.exports = new Config().extend("webpack/base.config.js").merge({
    output: {
        path: DIST_DIR
    },
    plugins: [
        new Copy({
            patterns: [{
                from: STATIC_DIR,
                to: DIST_DIR + "/static"
            }]
        })
    ]
})