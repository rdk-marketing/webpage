const {Config} = require("webpack-config")
const {SRC_DIR, STATIC_DIR} = process.env

module.exports = new Config().extend("webpack/base.config.js").merge({
    devServer: {
        contentBase: [SRC_DIR, STATIC_DIR],
        contentBasePublicPath: ["/", "/static"]
    }
})