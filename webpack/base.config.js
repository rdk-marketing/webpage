const CSSPlugin = require("mini-css-extract-plugin")
const HTMLPlugin = require("html-webpack-plugin")
const {Config} = require("webpack-config")
const path = require("path")
const {DefinePlugin} = require("webpack")
const {SRC_DIR, ROOT_DIR, NODE_ENV : mode} = process.env

const dotenv = require("dotenv").config( {
    path: path.join(ROOT_DIR, ".env")
})

module.exports = new Config().merge({
    mode,
    entry: {
        app: [path.join(SRC_DIR, "index.js")],
    },  
    output: {
        filename: "[name].js"
    },
    plugins: [
        new HTMLPlugin({template: path.join(SRC_DIR, "./index.pug"), filename: "index.html"}),
        new CSSPlugin(),
        new DefinePlugin({"process.env": dotenv.parsed})
    ],
    module: {
        rules: [
            {
                test: /\.pug$/,
                use: ["pug-loader"]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [CSSPlugin.loader, "css-loader", "sass-loader"]
            },
            {
                test: /\.svg$/,
                use: ["html-loader"]
            }
        ]
    }
})
