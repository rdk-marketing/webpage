const {Config, environment} = require("webpack-config")
const path = require("path")

const ROOT_DIR = process.env.ROOT_DIR = __dirname
process.env.SRC_DIR = path.join(ROOT_DIR, "src")
process.env.STATIC_DIR = path.join(ROOT_DIR, "static")
process.env.DIST_DIR = path.join(ROOT_DIR, "dist")

environment.setAll({
    env: () => process.env.NODE_ENV
})

module.exports = new Config().extend("webpack/[env].config.js")
